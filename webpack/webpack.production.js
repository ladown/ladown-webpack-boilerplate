import ImageMinimizerPlugin from 'image-minimizer-webpack-plugin';
import { merge } from 'webpack-merge';

import WebpackCommonConfig from './webpack.common.js';

const WebpackProductionConfig = merge(WebpackCommonConfig, {
	devtool: false,

	mode: 'production',

	optimization: {
		minimizer: [
			'...',
			new ImageMinimizerPlugin({
				deleteOriginalAssets: false,
				exclude: /assets[\\/]img[\\/]icons/,
				generator: [
					{
						filter: () => true,
						implementation: ImageMinimizerPlugin.sharpGenerate,
						options: {
							encodeOptions: {
								webp: {
									mixed: true,
									quality: 90,
								},
							},
						},
						preset: 'webp',
					},
				],
				include: /assets[\\/]img/,
				minimizer: [
					{
						implementation: ImageMinimizerPlugin.sharpMinify,
						options: {
							encodeOptions: {
								avif: {
									quality: 90,
								},
								jpeg: {
									progressive: true,
									quality: 90,
								},
								png: {
									compressionLevel: 5,
									progressive: true,
									quality: 90,
								},
								webp: {
									mixed: true,
									quality: 90,
								},
							},
						},
					},
					{
						implementation: ImageMinimizerPlugin.svgoMinify,
						options: {
							encodeOptions: {
								multipass: true,
								plugins: ['preset-default'],
							},
						},
					},
				],
				test: /\.(png|svg|jpe?g|webp|gif|ico)$/i,
			}),
			'...',
		],
		runtimeChunk: 'single',
		splitChunks: {
			cacheGroups: {
				vendors: {
					name: 'vendors',
					chunks: 'all',
					test: /[\\/]node_modules[\\/].+\.(js|ts)$/,
				},
			},
		},
	},
});

export default WebpackProductionConfig;
