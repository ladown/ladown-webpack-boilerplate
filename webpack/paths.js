import { platform } from 'os';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';

const normalizePath = (pathToNormalize) => {
	return platform() === 'win32' ? pathToNormalize.split('\\').join('/') : pathToNormalize;
};

const formattedDirname =
	platform() === 'win32' ? normalizePath(dirname(fileURLToPath(import.meta.url))) : dirname(fileURLToPath(import.meta.url));

const Paths = {
	build: {
		default: path.posix.join(formattedDirname, '../', 'build/'),
		uploads: path.posix.join(formattedDirname, '../', 'build/assets/uploads'),
	},

	formattedDirname,

	normalizePath,

	src: {
		default: path.posix.join(formattedDirname, '../', 'src/'),
		fonts: path.posix.join(formattedDirname, '../', 'src/assets/fonts/'),
		icons: path.posix.join(formattedDirname, '../', 'src/assets/img/icons/'),
		images: path.posix.join(formattedDirname, '../', 'src/assets/img/'),
		pugPages: path.posix.join(formattedDirname, '../', 'src/views/pages/'),
		scripts: path.posix.join(formattedDirname, '../', 'src/assets/scripts/'),
		static: path.posix.join(formattedDirname, '../', 'src/static/'),
		styles: path.posix.join(formattedDirname, '../', 'src/assets/styles/'),
		uploads: path.posix.join(formattedDirname, '../', 'src/assets/uploads/'),
		videos: path.posix.join(formattedDirname, '../', 'src/assets/video/'),
	},

	static: path.join(formattedDirname, '../', 'src/static/'),
};

export default Paths;
