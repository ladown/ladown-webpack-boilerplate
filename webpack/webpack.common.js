/* eslint-disable no-nested-ternary */
import CopyPlugin from 'copy-webpack-plugin';
import Dotenv from 'dotenv-webpack';
import path from 'path';
import PugPlugin from 'pug-plugin';
import SVGSpriteLoaderPlugin from 'svg-sprite-loader/plugin.js';

import postcssPlugins from '../postcss.config.js';
import Paths from './paths.js';
import { generateWebpackEntries, setPageList, settings } from './utils.js';

const WebpackResolveSettings = {
	alias: {
		'@components': path.posix.join(Paths.formattedDirname, '../', 'src/components/'),
		'@fonts': path.posix.join(Paths.formattedDirname, '../', 'src/assets/fonts/'),
		'@icons': path.posix.join(Paths.formattedDirname, '../', 'src/assets/img/icons/'),
		'@images': path.posix.join(Paths.formattedDirname, '../', 'src/assets/img/'),
		'@root': path.posix.join(Paths.formattedDirname, '../', ''),
		'@scripts': path.posix.join(Paths.formattedDirname, '../', 'src/assets/scripts/'),
		'@src': path.posix.join(Paths.formattedDirname, '../', 'src/'),
		'@styles': path.posix.join(Paths.formattedDirname, '../', 'src/assets/styles/'),
		'@uploads': path.posix.join(Paths.formattedDirname, '../', 'src/assets/uploads/'),
		'@videos': path.posix.join(Paths.formattedDirname, '../', 'src/assets/video/'),
		'@views': path.posix.join(Paths.formattedDirname, '../', 'src/views/'),
	},
};

const WebpackGlobImportLoader = {
	loader: 'glob-import-loader',
	options: {
		resolve: WebpackResolveSettings,
	},
};

const WebpackCommonConfig = {
	entry: generateWebpackEntries(),

	externals: {
		ymaps3: 'ymaps3',
	},

	module: {
		rules: [
			{
				oneOf: [
					{
						issuer: /\.(js|ts)$/,
						loader: PugPlugin.loader,
						options: {
							method: 'compile',
						},
					},
					{
						loader: PugPlugin.loader,
					},
				],
				test: /\.pug$/,
			},

			{
				exclude: [/node_modules/, /bower_components/],
				resolve: {
					fullySpecified: false,
				},
				test: /\.m?js$/,
				use: ['babel-loader', WebpackGlobImportLoader],
			},

			{
				test: /\.(c|sa|sc)ss$/i,
				use: [
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: postcssPlugins,
							},
						},
					},
					'sass-loader',
					WebpackGlobImportLoader,
				],
			},

			{
				include: /assets[\\/]img[\\/]icons/,
				test: /\.svg$/,
				use: [
					{
						loader: 'svg-sprite-loader',
						options: {
							extract: true,
							outputPath: 'assets/img/icons/',
							publicPath: 'auto',
						},
					},
					'svg-transform-loader',
					{
						loader: 'svgo-loader',
						options: {
							plugins: [
								'preset-default',
								{
									name: 'removeAttrs',
									params: {
										attrs: '(fill|stroke|stroke-width|stroke-linecap|stroke-linejoin|fill-opacity|fill-rule|stroke-opacity)',
									},
								},
							],
						},
					},
				],
			},

			{
				exclude: /assets[\\/]img[\\/]icons/,
				resourceQuery: /inline/,
				test: /\.(svg)$/i,
				type: 'asset/inline',
			},

			{
				exclude: [/assets[\\/]img[\\/]icons/],
				generator: {
					filename: ({ filename }) => {
						const { dir } = path.posix.parse(filename);
						const outputPath = dir.replace('src/', '');
						return settings.isHashedMode
							? `${outputPath}/[name].[hash:8][ext]`
							: settings.isProduction
							  ? `${outputPath}/[name][ext]`
							  : `${outputPath}/[name].[hash:8][ext]`;
					},
				},
				resourceQuery: {
					not: [/inline/],
				},
				test: /\.(woff2?|ttf|otf|eot|svg|png|jpe?g|webp|gif|ico|mp3|ogg|wav|mp4|ogv|webm|json|pdf)$/,
				type: 'asset/resource',
			},
		],
	},

	output: {
		chunkFilename: settings.isHashedMode
			? 'assets/js/[name].[id].[contenthash:8].js'
			: settings.isProduction
			  ? 'assets/js/[name].[id].js'
			  : 'assets/js/[name].[id].[contenthash:8].js',
		clean: true,
		filename: settings.isHashedMode
			? 'assets/js/[name].[contenthash:8].js'
			: settings.isProduction
			  ? 'assets/js/[name].js'
			  : 'assets/js/[name].[contenthash:8].js',
		path: Paths.build.default,
		publicPath: 'auto',
	},

	plugins: [
		new CopyPlugin({
			patterns: [
				{
					from: Paths.src.static,
					noErrorOnMissing: true,
					to: Paths.build.default,
				},
			],
		}),

		new PugPlugin({
			css: {
				filename: settings.isHashedMode
					? 'assets/css/[name].[contenthash:8].css'
					: settings.isProduction
					  ? 'assets/css/[name].css'
					  : 'assets/css/[name].[contenthash:8].css',
			},

			postprocess(content, { assetFile }) {
				if (assetFile.includes('page-list')) {
					const newContent = setPageList(content);

					return newContent;
				}
				const newContent = content.replaceAll(
					/(?:^|[^а-яёА-ЯЁ0-9_])(в|без|а|до|из|к|я|на|по|о|от|перед|при|через|с|у|за|над|об|под|про|для|и|или|со|около|между)(?:^|[^а-яёА-ЯЁ0-9_])/g,
					(match) => {
						return match.slice(-1) === ' ' ? `${match.substr(0, match.length - 1)}&nbsp;` : match;
					},
				);

				return newContent;
			},
		}),

		new SVGSpriteLoaderPlugin({
			plainSprite: true,
		}),

		new Dotenv(),
	],

	resolve: WebpackResolveSettings,

	target: 'web',
};

export default WebpackCommonConfig;
