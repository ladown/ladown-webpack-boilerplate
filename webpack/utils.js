import fs from 'fs';
import { globSync } from 'glob';
import path from 'path';

import packageJson from '../package.json' assert { type: 'json' };
import Paths from './paths.js';

export const settings = {
	isHashedMode: true,
	isProduction: process.argv.includes('mode=production'),
};

export const capitalizeFirstLetter = (string) => {
	return `${string[0].toUpperCase()}${string.slice(1)}`;
};

export const capitilizeWords = (string) => {
	return string
		.split(' ')
		.map((word) => `${word[0].toUpperCase()}${word.slice(1)}`)
		.join(' ');
};

export const getProjectName = () => {
	const packageName = packageJson.name.replace(/-|_/gm, ' ').toLowerCase().trim();
	return capitilizeWords(packageName);
};

export const generateWebpackEntries = () => {
	return globSync(`${Paths.src.pugPages}/**/*.pug`).reduce((acc, filePath) => {
		const normalizedFilePath = Paths.normalizePath(filePath);
		const entryName = path.posix.parse(normalizedFilePath).name;

		acc[entryName] = filePath;

		return acc;
	}, {});
};

export const getPugPages = () => {
	return globSync(`${Paths.src.pugPages}/**/*.pug`).map((filePath) => {
		const normalizedFilePath = Paths.normalizePath(filePath);
		const pageName = path.posix.parse(normalizedFilePath).name;

		return { filePath, pageName };
	});
};

export const generatePagesList = () => {
	const pugFiles = getPugPages();
	let list = '<ol class="page-list__items">';

	pugFiles.forEach(({ filePath, pageName }) => {
		if (!pageName.includes('page-list')) {
			const content = fs.readFileSync(filePath, 'utf8');
			const titleBlock = content.match(/title = '(.*?)'/g);
			const fileName = titleBlock && titleBlock.length ? titleBlock.toString().replace(/(^title = '|'$)/g, '') : pageName;
			const href = pageName === 'index' ? './' : `/${pageName}.html`;
			// const href =
			// 	pageName === 'index.pug'
			// 		? `https://portfolio.ermilovee.ru/project-name/`
			// 		: `https://portfolio.ermilovee.ru/project-name/${pageName.replace('.pug', '.html')}`;

			list += `<li class="page-list__item"><a href="${href}" class="page-list__link" target="_blank">${capitilizeWords(
				fileName,
			)}</a></li>`;
		}
	});

	list += '</ol>';

	return list;
};

export const setPageList = (content) => {
	const projectName = getProjectName();
	const pageList = generatePagesList();
	let newContent = content.replace(/<div id="page-list"><\/div>/g, pageList);
	newContent = newContent.replaceAll('#project-name', projectName);
	return newContent;
};
