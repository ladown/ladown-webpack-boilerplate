import ImageMinimizerPlugin from 'image-minimizer-webpack-plugin';
import { merge } from 'webpack-merge';

import Paths from './paths.js';
import WebpackCommonConfig from './webpack.common.js';

const WebpackDevelopmentConfig = merge(WebpackCommonConfig, {
	devServer: {
		compress: true,
		historyApiFallback: true,
		hot: true,
		liveReload: true,
		open: '/page-list.html',
		static: {
			directory: Paths.static,
		},
		watchFiles: {
			options: {
				usePolling: true,
			},
			paths: ['src/**/*.*'],
		},
	},

	devtool: 'eval',

	mode: 'development',

	plugins: [
		new ImageMinimizerPlugin({
			deleteOriginalAssets: false,
			exclude: /assets[\\/]img[\\/]icons/,
			generator: [
				{
					filter: () => true,
					implementation: ImageMinimizerPlugin.sharpGenerate,
					options: {
						encodeOptions: {
							webp: {
								quality: 100,
							},
						},
					},
					preset: 'webp',
				},
			],
			include: /assets[\\/]img/,
			test: /\.(png|svg|jpe?g|webp|gif|ico)$/i,
		}),
	],
});

export default WebpackDevelopmentConfig;
