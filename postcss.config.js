// #TODO PostCss config for es6 syntax https://github.com/postcss/postcss/issues/1771

import autoprefixer from 'autoprefixer';
import postcssMergeLonghand from 'postcss-merge-longhand';
import postcssPresetEnv from 'postcss-preset-env';
import postcssSortMediaQueries from 'postcss-sort-media-queries';

import { settings } from './webpack/utils.js';

export default [
	postcssSortMediaQueries({
		sort: 'desktop-first',
	}),
	...(settings.isProduction
		? [postcssPresetEnv(), postcssMergeLonghand(), autoprefixer({ cascade: true, grid: 'autoplace' })]
		: []),
];
