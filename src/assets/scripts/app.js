import EventBus from 'js-event-bus';

import '@icons/**/*.svg';
import Default from '@scripts/classes/default.class';

const AppEventBus = new EventBus();
window.AppEventBus = AppEventBus;

window.addEventListener('DOMContentLoaded', () => {
	class App {
		constructor() {
			this.elements = {
				documentRoot: document.getElementsByTagName('html')[0],
			};
		}

		canUseWebp() {
			const canvasElement = document.createElement('canvas');

			if (
				!(!canvasElement.getContext || !canvasElement.getContext('2d')) &&
				canvasElement.toDataURL('image/webp').indexOf('data:image/webp') === 0
			) {
				this.elements.documentRoot.classList.add('webp');
			} else {
				this.elements.documentRoot.classList.add('no-webp');
			}
		}

		init() {
			this.canUseWebp();

			this.initClasses();
			this.initModules();

			console.log('App has been initialized');

			return this;
		}

		initClasses() {
			new Default().init();
		}

		initModules() {}
	}

	new App().init();
});
