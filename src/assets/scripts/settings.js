export const mediaBreakpoints = {
	max: {
		hd: 1920,
		largeDesktop: 1440,
		largeMobile: 580,
		largeTablet: 1120,
		macbook: 1700,
		smallDesktop: 1280,
		smallMobile: 440,
		smallTablet: 820,
	},

	min: {
		hd: 1921,
		largeDesktop: 1441,
		largeMobile: 581,
		largeTablet: 1121,
		macbook: 1701,
		smallDesktop: 1281,
		smallMobile: 441,
		smallTablet: 821,
	},
};

export const defaultAnimationEasing = 'cubic-bezier(0.45, 0, 0.55, 1)';
export const defaultAnimationDuration = 0.3;

export const colorBlack = '#000';
